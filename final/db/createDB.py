#Use this script to create the database tables
#This script does NOT populate the tables with any data

import mysql.connector

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'DatSubway'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST)
cursor = cnx.cursor()

###################################
## Create DB if it doesn't exist ##
###################################

createDB = (("CREATE DATABASE IF NOT EXISTS %s DEFAULT CHARACTER SET latin1") % (DATABASE_NAME))
cursor.execute(createDB)

#########################
##    Switch to DB     ##
#########################

useDB = (("USE %s") % (DATABASE_NAME))
cursor.execute(useDB)

#############################
## Drop all table(s) first ##
#############################

dropTableQuery = ("DROP TABLE IF EXISTS items")
cursor.execute(dropTableQuery)

##########################
## Create table(s) next ##
##########################

createTableQuery = ('''CREATE TABLE items (
						id INT NOT NULL AUTO_INCREMENT,
						type enum ('vegetables','condiments','breads','cheese','meats') NOT NULL,
						name VARCHAR(45) NOT NULL,
						weight INT NOT NULL,
						calories INT NOT NULL,
						fatcals INT NOT NULL, 
						fat FLOAT NOT NULL, 
						saturatedFat FLOAT NOT NULL, 
						transFat FLOAT NOT NULL, 
						cholesterol INT NOT NULL, 
						sodium INT NOT NULL, 
						carbos INT NOT NULL, 
						fibers INT NOT NULL, 
						sugars INT NOT NULL, 
						protein INT NOT NULL, 
						vitaminA INT NOT NULL, 
						vitaminC INT NOT NULL, 
						calcium INT NOT NULL, 
						iron INT NOT NULL,
						
						PRIMARY KEY (id))'''
					)
cursor.execute(createTableQuery)

#Commit the data and close the connection to MySQL
cnx.commit()
cnx.close()
