''' Controller for /restaurants/{id}
	Imported from handler for /restaurants '''
import cherrypy
import apiutil
from categories import Categories
import mysql.connector
import json

class RestaurantID(object):
	''' Handles resource /restaurants/{id} 
		Allowed methods: GET, PUT, DELETE, OPTIONS '''
	exposed = True

	def __init__(self):
		self.categories=Categories()
		self.db=dict()
		self.db['name']='feednd'
		self.db['user']='root'
		self.db['host']='127.0.0.1'

	def GET(self, restID):
		''' Return information on restaurant restID'''
		
		q = "select restId, name, address, city, state, zip, phone, url from restaurants where restId='%s';" % restID
		
		try:
			cnx = mysql.connector.connect(
				user=self.db['user'],
				host=self.db['host'],
				database=self.db['name'],
			)
			cursor = cnx.cursor(dictionary=True)
			cursor.execute(q)
		except:
			return apiutil.errorJSON(0, "Error in the connection to the DB");
		
		result = cursor.fetchone()
		
		if(result == None):
			return apiutil.errorJSON(1, "No such Restaurant with this ID");
		
		result['errors'] = dict();
		
		return json.dumps(result, encoding='utf-8')

	def PUT(self, restID, **kwargs):
		''' Update restaurant with restID'''
		result = "PUT /restaurants/{id=%s}	  ...	 RestaurantID.PUT\n" % restID
		result += "PUT /restaurants body:\n"
		for key, value in kwargs.items():
			result+= "%s = %s \n" % (key,value)
		# Validate form data
		# Insert or update restaurant
		# Prepare response
		return result

	def DELETE(self, restID):
		''' Delete restaurant with restID'''
		#Validate restID
		#Delete restaurant
		#Prepare response
		return "DELETE /restaurants/{id=%s}   ...   RestaurantID.DELETE" % restID

	def OPTIONS(self, restID):
		''' Allows GET, PUT, DELETE, OPTIONS '''
		#Prepare response
		return "<p>/restaurants/{id} allows GET, PUT, DELETE, and OPTIONS</p>"
