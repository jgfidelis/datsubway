''' Implements handler for /menus
Imported from handler for /restaurants/{id} '''

import os, os.path, json, logging, mysql.connector
import apiutil
from apiutil import errorJSON
import cherrypy
from jinja2 import Environment, FileSystemLoader


env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))

class BreadID(object):
    ''' Handles resources /menus/{restID}
        Allowed methods: GET, POST, PUT, DELETE '''
    exposed = True

    def __init__(self):
        self.db=dict()
        self.db['name']='DatSubway'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def getDataFromDB(self, breadid):
        cnx = mysql.connector.connect(
            user=self.db['user'],
            host=self.db['host'],
            database=self.db['name'],
        )

        cursor = cnx.cursor()
        qn="select name, weight, calories, fatcals, fat, saturatedFat, transFat, cholesterol, sodium, carbos, fibers, sugars, protein, vitaminA, vitaminC, calcium, iron from items where type=\"breads\" and id=%s;" % breadid
        cursor.execute(qn)
        bread = cursor.fetchone()
        return bread

    def GET(self, breadid):
        ''' Return list of menus for restaurant restID'''

        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        cnx = mysql.connector.connect(
            user=self.db['user'],
            host=self.db['host'],
            database=self.db['name'],
        )
        cursor = cnx.cursor()

        q="select exists(select 1 from items where id=%s and type=\"breads\");" % (breadid)
        cursor.execute(q)
        if not cursor.fetchall()[0][0]:
            return errorJSON(code=1000, message="Bread with BreadID=%s does not exist" % (breadid))

        try:
           bread = self.getDataFromDB(breadid)
        except mysql.connector.Error as e:
            logging.error(e)
            raise

        if output_format == 'text/html':
            data = {"name":bread[0],
                    "weight":bread[1],
                    "calories":bread[2],
                    "fatcals":bread[3],
                    "fat":bread[4],
                    "saturatedFat":bread[5],
                    "transFat":bread[6],
                    "cholesterol":bread[7],
                    "sodium":bread[8],
                    "carbos":bread[9],
                    "fibers":bread[10],
                    "sugars":bread[11],
                    "protein":bread[12],
                    "vitaminA":bread[13],
                    "vitaminC":bread[14],
                    "calcium":bread[15],
                    "iron":bread[16]
                    }
            data["output"] = "success"
            return json.dumps(data, encoding='utf-8')
        else:
            data = {"name":bread[0],
                    "weight":bread[1],
                    "calories":bread[2],
                    "fatcals":bread[3],
                    "fat":bread[4],
                    "saturatedFat":bread[5],
                    "transFat":bread[6],
                    "cholesterol":bread[7],
                    "sodium":bread[8],
                    "carbos":bread[9],
                    "fibers":bread[10],
                    "sugars":bread[11],
                    "protein":bread[12],
                    "vitaminA":bread[13],
                    "vitaminC":bread[14],
                    "calcium":bread[15],
                    "iron":bread[16]
                    }
            data["output"] = "success"
            return json.dumps(data, encoding='utf-8') #MODIFICADO

    def OPTIONS(self, breadid):
        return "<p>/restaurants/{restID}/menus/ allows GET, POST, and OPTIONS</p>"
