''' Implements handler for /menus
Imported from handler for /restaurants/{id} '''

import os, os.path, json, logging, mysql.connector
import apiutil
from apiutil import errorJSON
import cherrypy
from jinja2 import Environment, FileSystemLoader

#from categoryid import CategoryID
from cheeseid import CheeseID

env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))

class Cheese(object):
    ''' Handles resources /menus/{restID}
        Allowed methods: GET, POST, PUT, DELETE '''
    exposed = True

    def __init__(self):
        self.id = CheeseID()
        self.db=dict()
        self.db['name']='DatSubway'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def _cp_dispatch(self,vpath):
        print "Home._cp_dispatch with vpath: %s \n" % vpath
        #return vpath
        if len(vpath) == 1: # /{type}
            foodid = str(vpath.pop(0))
            cherrypy.request.params['cheeseid']=foodid
            return self.id
        else:
            return vpath

    def getDataFromDB(self):
        cnx = mysql.connector.connect(
            user=self.db['user'],
            host=self.db['host'],
            database=self.db['name'],
        )
        cursor = cnx.cursor()
        qn="select id, name, weight, calories, fatcals, fat, saturatedFat, transFat, cholesterol, sodium, carbos, fibers, sugars, protein, vitaminA, vitaminC, calcium, iron from items where type=\"cheese\";" #ELE TRABALHA COM id =0, 1, O MEU EH O COMPLEXO
        #return str(restID)
        cursor.execute(qn)
        cheeses = cursor.fetchall()
        return cheeses

    def GET(self):
        ''' Return list of menus for restaurant restID'''

        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        try:
           cheeses = self.getDataFromDB()
        except mysql.connector.Error as e:
            logging.error(e)
            raise

        if output_format == 'text/html':
            a=1
        else:
            data = {}
            temp = []
            for cheese in cheeses:
                tempCheese = {"id":cheese[0],
                                "name":cheese[1],
                                "weight":cheese[2],
                                "calories":cheese[3],
                                "fatcals":cheese[4],
                                "fat":cheese[5],
                                "saturatedFat":cheese[6],
                                "transFat":cheese[7],
                                "cholesterol":cheese[8],
                                "sodium":cheese[9],
                                "carbos":cheese[10],
                                "fibers":cheese[11],
                                "sugars":cheese[12],
                                "protein":cheese[13],
                                "vitaminA":cheese[14],
                                "vitaminC":cheese[15],
                                "calcium":cheese[16],
                                "iron":cheese[17]
                                }
                temp.append(tempCheese)

            data["entries"] = temp
            data["output"] = "success"
            return json.dumps(data, encoding='utf-8') #MODIFICADO

    def OPTIONS(self):
        return "<p>/restaurants/{restID}/menus/ allows GET, POST, and OPTIONS</p>"
