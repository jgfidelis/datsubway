''' Implements handler for /menus
Imported from handler for /restaurants/{id} '''

import os, os.path, json, logging, mysql.connector
import apiutil
from apiutil import errorJSON
import cherrypy
from jinja2 import Environment, FileSystemLoader

#from categoryid import CategoryID
from veggieid import VeggieID

env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))

class Veggies(object):
    ''' Handles resources /menus/{restID}
        Allowed methods: GET, POST, PUT, DELETE '''
    exposed = True

    def __init__(self):
        self.id = VeggieID()
        self.db=dict()
        self.db['name']='DatSubway'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def _cp_dispatch(self,vpath):
        print "Home._cp_dispatch with vpath: %s \n" % vpath
        #return vpath
        if len(vpath) == 1: # /{type}
            foodid = str(vpath.pop(0))
            cherrypy.request.params['veggieid']=foodid
            return self.id
        else:
            return vpath

    def getDataFromDB(self):
        cnx = mysql.connector.connect(
            user=self.db['user'],
            host=self.db['host'],
            database=self.db['name'],
        )
        cursor = cnx.cursor()
        qn="select id, name, weight, calories, fatcals, fat, saturatedFat, transFat, cholesterol, sodium, carbos, fibers, sugars, protein, vitaminA, vitaminC, calcium, iron from items where type=\"vegetables\";" #ELE TRABALHA COM id =0, 1, O MEU EH O COMPLEXO
        #return str(restID)
        cursor.execute(qn)
        veggies = cursor.fetchall()
        return veggies

    def GET(self):
        ''' Return list of menus for restaurant restID'''

        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        try:
           veggies = self.getDataFromDB()
        except mysql.connector.Error as e:
            logging.error(e)
            raise

        if output_format == 'text/html':
            a=1
        else:
            data = {}
            temp = []
            for veggie in veggies:
                tempVeggie = {"id":veggie[0],
                                "name":veggie[1],
                                "weight":veggie[2],
                                "calories":veggie[3],
                                "fatcals":veggie[4],
                                "fat":veggie[5],
                                "saturatedFat":veggie[6],
                                "transFat":veggie[7],
                                "cholesterol":veggie[8],
                                "sodium":veggie[9],
                                "carbos":veggie[10],
                                "fibers":veggie[11],
                                "sugars":veggie[12],
                                "protein":veggie[13],
                                "vitaminA":veggie[14],
                                "vitaminC":veggie[15],
                                "calcium":veggie[16],
                                "iron":veggie[17]
                                }
                temp.append(tempVeggie)

            data["entries"] = temp
            data["output"] = "success"
            return json.dumps(data, encoding='utf-8') #MODIFICADO

    def OPTIONS(self):
        return "<p>/restaurants/{restID}/menus/ allows GET, POST, and OPTIONS</p>"
