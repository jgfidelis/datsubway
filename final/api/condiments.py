''' Implements handler for /menus
Imported from handler for /restaurants/{id} '''

import os, os.path, json, logging, mysql.connector
import apiutil
from apiutil import errorJSON
import cherrypy
from jinja2 import Environment, FileSystemLoader

#from categoryid import CategoryID
from condimentid import CondimentID

env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))

class Condiments(object):
    ''' Handles resources /menus/{restID}
        Allowed methods: GET, POST, PUT, DELETE '''
    exposed = True

    def __init__(self):
        self.id = CondimentID()
        self.db=dict()
        self.db['name']='DatSubway'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def _cp_dispatch(self,vpath):
        print "Home._cp_dispatch with vpath: %s \n" % vpath
        #return vpath
        if len(vpath) == 1: # /{type}
            foodid = str(vpath.pop(0))
            cherrypy.request.params['condimentid']=foodid
            return self.id
        else:
            return vpath

    def getDataFromDB(self):
        cnx = mysql.connector.connect(
            user=self.db['user'],
            host=self.db['host'],
            database=self.db['name'],
        )
        cursor = cnx.cursor()
        qn="select id, name, weight, calories, fatcals, fat, saturatedFat, transFat, cholesterol, sodium, carbos, fibers, sugars, protein, vitaminA, vitaminC, calcium, iron from items where type=\"condiments\";" #ELE TRABALHA COM id =0, 1, O MEU EH O COMPLEXO
        #return str(restID)
        cursor.execute(qn)
        condiments = cursor.fetchall()
        return condiments

    def GET(self):
        ''' Return list of menus for restaurant restID'''

        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        try:
           condiments = self.getDataFromDB()
        except mysql.connector.Error as e:
            logging.error(e)
            raise

        if output_format == 'text/html':
            a=1
        else:
            data = {}
            temp = []
            for condiment in condiments:
                tempCond = {"id":condiment[0],
                                "name":condiment[1],
                                "weight":condiment[2],
                                "calories":condiment[3],
                                "fatcals":condiment[4],
                                "fat":condiment[5],
                                "saturatedFat":condiment[6],
                                "transFat":condiment[7],
                                "cholesterol":condiment[8],
                                "sodium":condiment[9],
                                "carbos":condiment[10],
                                "fibers":condiment[11],
                                "sugars":condiment[12],
                                "protein":condiment[13],
                                "vitaminA":condiment[14],
                                "vitaminC":condiment[15],
                                "calcium":condiment[16],
                                "iron":condiment[17]
                                }
                temp.append(tempCond)

            data["entries"] = temp
            data["output"] = "success"
            return json.dumps(data, encoding='utf-8') #MODIFICADO

    def OPTIONS(self):
        return "<p>/restaurants/{restID}/menus/ allows GET, POST, and OPTIONS</p>"
