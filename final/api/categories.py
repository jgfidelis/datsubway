''' Implements handler for /menus
Imported from handler for /restaurants/{id} '''

import os, os.path, json, logging, mysql.connector

import cherrypy
from jinja2 import Environment, FileSystemLoader

from categoryid import CategoryID

env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))

class Categories(object):
	''' Handles resources /menus/{restID}
		Allowed methods: GET, POST, PUT, DELETE '''
	exposed = True

	def __init__(self):
		self.id = CategoryID()
		self.db=dict()
		self.db['name']='feednd'
		self.db['user']='root'
		self.db['host']='127.0.0.1'

	def getDataFromDB(self,id):
		cnx = mysql.connector.connect(
			user=self.db['user'],
			host=self.db['host'],
			database=self.db['name'],
		)
		cursor = cnx.cursor()
		qn="select name from restaurants where restId='%s'" % id
		cursor.execute(qn)
		restName=cursor.fetchone()[0]
		q="select menuId, name from menus where restId='%s' order by name" % id
		cursor.execute(q)
		result=cursor.fetchall()
		return restName,result

	def GET(self, restID):
		''' Return list of categories for restaurant restID'''

		# Return data in the format requested in the Accept header
		# Fail with a status of 406 Not Acceptable if not HTML or JSON
		output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])
		
		try:
			restName,result=self.getDataFromDB(restID)
		except mysql.connector.Error as e:
			logging.error(e)
			raise

		if output_format == 'text/html':
			return env.get_template('categories-tmpl.html').render(
				rID=restID,
				rName=restName,
				categories=result,
				base=cherrypy.request.base.rstrip('/') + '/'
			)
		else:
			data = [{
				'href': 'restaurants/%s/menus/%s/items' % (restID, category_id),
				'name': category_name
			} for category_id, category_name in result]
			return json.dumps(data, encoding='utf-8')

	def POST(self, **kwargs):
		result= "POST /restaurants/{restID}/menus	 ...	 Categories.POST\n"
		result+= "POST /restaurants/{restID}/menus body:\n"
		for key, value in kwargs.items():
			result+= "%s = %s \n" % (key,value)
		# Validate form data
		# Insert restaurant
		# Prepare response
		return result

	def OPTIONS(self,restID):
		return "<p>/restaurants/{restID}/menus/ allows GET, POST, and OPTIONS</p>"
