import apiutil
from apiutil import errorJSON
import sys
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import threading
import cherrypy
import os
import os.path
import json
import mysql.connector
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
import logging


class Sub(object):

	exposed = True

	def __init__(self):
		self.db = dict()
		self.db['name']='DatSubway'
		self.db['user']='root'
		self.db['host']='127.0.0.1'

	def POST(self):
		output = {"output":"success"}

		cnx = mysql.connector.connect(
			user=self.db['user'],
			host=self.db['host'],
			database=self.db['name'],
		)
		cursor = cnx.cursor()
		output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])
		if output_format == 'text/html':
			try:
				data = cherrypy.request.body.fp.read()
				processed_data = json.loads(data)
				
				breadID = int(processed_data['breadID'])
			except:
				return errorJSON(code=2000, message="Expected breadID for input")
			try:
				footlong = str(processed_data["footlong"])
			except:
				return errorJSON(code=2001, message="Expected footlong for input")
			try:
				cheeseID = int(processed_data["cheeseID"])
			except:
				return errorJSON(code=2002, message="Expected cheese for input")
			try:
				veggiesID = processed_data["veggiesID"]
				veggiesList = []
				for veggieID in veggiesID:
					veggieID = int(veggieID)
					veggiesList.append(veggieID)
			except:
				return errorJSON(code=2003, message="Expected veggiesID for input or unexpected value in list")
			try:
				condimentsID = processed_data["condimentsID"]
				condsList = []
				for condID in condimentsID:
					condID = int(condID)
					condsList.append(condID)
			except:
				return errorJSON(code=2004, message="Expected condimentsID for input")
			try:
				meatID = int(processed_data["meatID"])
			except:
				return errorJSON(code=2005, message="Expected meatID for input")



			allItems=[]

			
			q="select exists(select 1 from items where id=%s and type=\"breads\");" % (breadID)
			cursor.execute(q)
			if not cursor.fetchall()[0][0]:
				return errorJSON(code=1000, message="Bread with BreadID=%s does not exist" % (breadID))		
			qn="select name, weight, calories, fatcals, fat, saturatedFat, transFat, cholesterol, sodium, carbos, fibers, sugars, protein, vitaminA, vitaminC, calcium, iron from items where type=\"breads\" and id=%s;" % breadID
			cursor.execute(qn)
			bread = cursor.fetchone()
			allItems.append(bread)

			q="select exists(select 1 from items where id=%s and type=\"cheese\");" % (cheeseID)
			cursor.execute(q)
			if not cursor.fetchall()[0][0]:
				return errorJSON(code=1001, message="Cheese with CheeseID=%s does not exist" % (cheeseID))
			qn="select name, weight, calories, fatcals, fat, saturatedFat, transFat, cholesterol, sodium, carbos, fibers, sugars, protein, vitaminA, vitaminC, calcium, iron from items where type=\"cheese\" and id=%s;" % cheeseID
			cursor.execute(qn)
			cheese = cursor.fetchone()
			allItems.append(cheese)

			q="select exists(select 1 from items where id=%s and type=\"meats\");" % (meatID)
			cursor.execute(q)
			if not cursor.fetchall()[0][0]:
				return errorJSON(code=1003, message="Meat with MeatID=%s does not exist" % (meatID))
			qn="select name, weight, calories, fatcals, fat, saturatedFat, transFat, cholesterol, sodium, carbos, fibers, sugars, protein, vitaminA, vitaminC, calcium, iron from items where type=\"meats\" and id=%s;" % meatID
			cursor.execute(qn)
			meat = cursor.fetchone()
			allItems.append(meat)

			for veggieID in veggiesList:
				q="select exists(select 1 from items where id=%s and type=\"vegetables\");" % (veggieID)
				cursor.execute(q)
				if not cursor.fetchall()[0][0]:
					return errorJSON(code=1004, message="Veggie with VeggieID=%s does not exist" % (veggieID))
				qn="select name, weight, calories, fatcals, fat, saturatedFat, transFat, cholesterol, sodium, carbos, fibers, sugars, protein, vitaminA, vitaminC, calcium, iron from items where type=\"vegetables\" and id=%s;" % veggieID
				cursor.execute(qn)
				veggie = cursor.fetchone()
				#veggiesObjects.append(veggie)
				allItems.append(veggie)
			
			for condID in condsList:
				q="select exists(select 1 from items where id=%s and type=\"condiments\");" % (condID)
				cursor.execute(q)
				if not cursor.fetchall()[0][0]:
					return errorJSON(code=1002, message="Condiment with CondimentID=%s does not exist" % (condID))
				qn="select name, weight, calories, fatcals, fat, saturatedFat, transFat, cholesterol, sodium, carbos, fibers, sugars, protein, vitaminA, vitaminC, calcium, iron from items where type=\"condiments\" and id=%s;" % condID
				cursor.execute(qn)
				condiment = cursor.fetchone()
				#condsObjects.append(condiment)
				allItems.append(condiment)

			totalCalories = 0
			totalFatcals = 0
			totalFat = 0
			totalSaturatedFat = 0
			totalTransFat = 0
			totalCholesterol = 0
			totalSodium = 0
			totalCarbos = 0
			totalFibers = 0
			totalSugars = 0
			totalProtein = 0
			totalVitaminA = 0
			totalVitaminC = 0
			totalCalcium = 0
			totalIron = 0

			for item in allItems:
				totalCalories += int(item[2])
				totalFatcals += int(item[3])
				totalFat += int(item[4])
				totalSaturatedFat += float(item[5])
				totalTransFat += float(item[6])
				totalCholesterol += int(item[7])
				totalSodium += int(item[8])
				totalCarbos += int(item[9])
				totalFibers += int(item[10])
				totalSugars += int(item[11])
				totalProtein += int(item[12])
				totalVitaminA += int(item[13])
				totalVitaminC += int(item[14])
				totalCalcium += int(item[15])
				totalIron += int(item[16])

			if footlong == "true":
				totalCalories *= 2
				totalFatcals *= 2
				totalFat *= 2
				totalSaturatedFat *= 2
				totalTransFat *= 2
				totalCholesterol *= 2
				totalSodium *= 2
				totalCarbos *= 2
				totalFibers *= 2
				totalSugars *= 2
				totalProtein *= 2
				totalVitaminA *= 2
				totalVitaminC *= 2
				totalCalcium *= 2
				totalIron *= 2

			output = {'totalCalories':totalCalories,
				  'totalFatcals':totalFatcals,
				  'totalFat':totalFat,
				  'totalSaturatedFat':totalSaturatedFat,
				  'totalTransFat':totalTransFat,
				  'totalCholesterol':totalCholesterol,
				  'totalSodium':totalSodium,
				  'totalCarbos':totalCarbos,
				  'totalFibers':totalFibers,
				  'totalSugars':totalSugars,
				  'TotalProtein':totalProtein,
				  'totalVitaminA':totalVitaminA,
				  'totalVitaminC':totalVitaminC,
				  'totalIron':totalIron}
			output['result'] = 'success'
			return json.dumps(output, encoding="utf-8")
		else:
			try:
				data = cherrypy.request.body.fp.read()
				processed_data = json.loads(data)
				
				breadID = int(processed_data['breadID'])
			except:
				return errorJSON(code=2000, message="Expected breadID for input")
			try:
				footlong = str(processed_data["footlong"])
			except:
				return errorJSON(code=2001, message="Expected footlong for input")
			try:
				cheeseID = int(processed_data["cheeseID"])
			except:
				return errorJSON(code=2002, message="Expected cheese for input")
			try:
				veggiesID = processed_data["veggiesID"]
				veggiesList = []
				for veggieID in veggiesID:
					veggieID = int(veggieID)
					veggiesList.append(veggieID)
			except:
				return errorJSON(code=2003, message="Expected veggiesID for input or unexpected value in list")
			try:
				condimentsID = processed_data["condimentsID"]
				condsList = []
				for condID in condimentsID:
					condID = int(condID)
					condsList.append(condID)
			except:
				return errorJSON(code=2004, message="Expected condimentsID for input")
			try:
				meatID = int(processed_data["meatID"])
			except:
				return errorJSON(code=2005, message="Expected meatID for input")



			allItems=[]

			
			q="select exists(select 1 from items where id=%s and type=\"breads\");" % (breadID)
			cursor.execute(q)
			if not cursor.fetchall()[0][0]:
				return errorJSON(code=1000, message="Bread with BreadID=%s does not exist" % (breadID))		
			qn="select name, weight, calories, fatcals, fat, saturatedFat, transFat, cholesterol, sodium, carbos, fibers, sugars, protein, vitaminA, vitaminC, calcium, iron from items where type=\"breads\" and id=%s;" % breadID
			cursor.execute(qn)
			bread = cursor.fetchone()
			allItems.append(bread)

			q="select exists(select 1 from items where id=%s and type=\"cheese\");" % (cheeseID)
			cursor.execute(q)
			if not cursor.fetchall()[0][0]:
				return errorJSON(code=1001, message="Cheese with CheeseID=%s does not exist" % (cheeseID))
			qn="select name, weight, calories, fatcals, fat, saturatedFat, transFat, cholesterol, sodium, carbos, fibers, sugars, protein, vitaminA, vitaminC, calcium, iron from items where type=\"cheese\" and id=%s;" % cheeseID
			cursor.execute(qn)
			cheese = cursor.fetchone()
			allItems.append(cheese)

			q="select exists(select 1 from items where id=%s and type=\"meats\");" % (meatID)
			cursor.execute(q)
			if not cursor.fetchall()[0][0]:
				return errorJSON(code=1003, message="Meat with MeatID=%s does not exist" % (meatID))
			qn="select name, weight, calories, fatcals, fat, saturatedFat, transFat, cholesterol, sodium, carbos, fibers, sugars, protein, vitaminA, vitaminC, calcium, iron from items where type=\"meats\" and id=%s;" % meatID
			cursor.execute(qn)
			meat = cursor.fetchone()
			allItems.append(meat)

			for veggieID in veggiesList:
				q="select exists(select 1 from items where id=%s and type=\"vegetables\");" % (veggieID)
				cursor.execute(q)
				if not cursor.fetchall()[0][0]:
					return errorJSON(code=1004, message="Veggie with VeggieID=%s does not exist" % (veggieID))
				qn="select name, weight, calories, fatcals, fat, saturatedFat, transFat, cholesterol, sodium, carbos, fibers, sugars, protein, vitaminA, vitaminC, calcium, iron from items where type=\"vegetables\" and id=%s;" % veggieID
				cursor.execute(qn)
				veggie = cursor.fetchone()
				#veggiesObjects.append(veggie)
				allItems.append(veggie)
			
			for condID in condsList:
				q="select exists(select 1 from items where id=%s and type=\"condiments\");" % (condID)
				cursor.execute(q)
				if not cursor.fetchall()[0][0]:
					return errorJSON(code=1002, message="Condiment with CondimentID=%s does not exist" % (condID))
				qn="select name, weight, calories, fatcals, fat, saturatedFat, transFat, cholesterol, sodium, carbos, fibers, sugars, protein, vitaminA, vitaminC, calcium, iron from items where type=\"condiments\" and id=%s;" % condID
				cursor.execute(qn)
				condiment = cursor.fetchone()
				#condsObjects.append(condiment)
				allItems.append(condiment)

			totalCalories = 0
			totalFatcals = 0
			totalFat = 0
			totalSaturatedFat = 0
			totalTransFat = 0
			totalCholesterol = 0
			totalSodium = 0
			totalCarbos = 0
			totalFibers = 0
			totalSugars = 0
			totalProtein = 0
			totalVitaminA = 0
			totalVitaminC = 0
			totalCalcium = 0
			totalIron = 0

			for item in allItems:
				#return "BREAD:" + str(item[0])
				totalCalories += int(item[2])
				totalFatcals += int(item[3])
				totalFat += int(item[4])
				totalSaturatedFat += float(item[5])
				totalTransFat += float(item[6])
				totalCholesterol += int(item[7])
				totalSodium += int(item[8])
				totalCarbos += int(item[9])
				totalFibers += int(item[10])
				totalSugars += int(item[11])
				totalProtein += int(item[12])
				totalVitaminA += int(item[13])
				totalVitaminC += int(item[14])
				totalCalcium += int(item[15])
				totalIron += int(item[16])

			if footlong == "True":
				totalCalories *= 2
				totalFatcals *= 2
				totalFat *= 2
				totalSaturatedFat *= 2
				totalTransFat *= 2
				totalCholesterol *= 2
				totalSodium *= 2
				totalCarbos *= 2
				totalFibers *= 2
				totalSugars *= 2
				totalProtein *= 2
				totalVitaminA *= 2
				totalVitaminC *= 2
				totalCalcium *= 2
				totalIron *= 2

			output = {'totalCalories':totalCalories,
				  'totalFatcals':totalFatcals,
				  'totalFat':totalFat,
				  'totalSaturatedFat':totalSaturatedFat,
				  'totalTransFat':totalTransFat,
				  'totalCholesterol':totalCholesterol,
				  'totalSodium':totalSodium,
				  'totalCarbos':totalCarbos,
				  'totalFibers':totalFibers,
				  'totalSugars':totalSugars,
				  'TotalProtein':totalProtein,
				  'totalVitaminA':totalVitaminA,
				  'totalVitaminC':totalVitaminC,
				  'totalIron':totalIron}
			output['result'] = 'success'
			return json.dumps(output, encoding="utf-8")










