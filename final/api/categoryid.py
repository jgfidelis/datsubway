''' Implements handler for /menus/{id}
Imported from handler for /menus'''
import cherrypy
from items import Items
class CategoryID(object):
	''' Handles resource /menus/{restID}/{catID}
		Allowed methods: GET, PUT, DELETE '''
	exposed = True

	def __init__(self):
		self.items = Items()

	def GET(self, restID, catID):
		''' Return info of category id for restaurant id'''
		return "GET /restaurants/{restID=%s}/menus/{catID=%s}  ...   CategoryID.GET" % (restID,catID)

	def PUT(self, restID, catID, **kwargs):
		''' Update category id for restaurant id'''
		result = "PUT /restaurants/{restID=%s}/menus/{catID=%s}   ...   CategoryID.PUT\n" % (restID,catID)
		result += "PUT /restaurants/{restID=%s}/menus/{catID=%s} body:\n"
		for key, value in kwargs.items():
			result+= "%s = %s \n" % (key,value)
		# Validate form data
		# Insert or update restaurant
		# Prepare response
		return result

	def DELETE(self, restID, catID):
		#Validate id
		#Delete restaurant
		#Prepare response
		return "DELETE /restaurants/{restID=%s}/menus/{catID=%s}   ...   CategoryID.DELETE" % (restID,catID)

	def OPTIONS(self,restID, catID):
		return "<p>/restaurants/{restID}/menus/{catID} allows GET, PUT, DELETE, and OPTIONS</p>"

