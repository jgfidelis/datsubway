''' Implements handler for /menus
Imported from handler for /restaurants/{id} '''

import os, os.path, json, logging, mysql.connector
import apiutil
from apiutil import errorJSON
import cherrypy
from jinja2 import Environment, FileSystemLoader

#from categoryid import CategoryID
from breadid import BreadID

env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))

class Breads(object):
    ''' Handles resources /menus/{restID}
        Allowed methods: GET, POST, PUT, DELETE '''
    exposed = True

    def __init__(self):
        self.id = BreadID()
        self.db=dict()
        self.db['name']='DatSubway'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def _cp_dispatch(self,vpath):
        print "Home._cp_dispatch with vpath: %s \n" % vpath
        #return vpath
        if len(vpath) == 1: # /{type}
            foodid = str(vpath.pop(0))
            cherrypy.request.params['breadid']=foodid
            return self.id
        else:
            return vpath

    def getDataFromDB(self):
        cnx = mysql.connector.connect(
            user=self.db['user'],
            host=self.db['host'],
            database=self.db['name'],
        )
        cursor = cnx.cursor()
        qn="select id, name, weight, calories, fatcals, fat, saturatedFat, transFat, cholesterol, sodium, carbos, fibers, sugars, protein, vitaminA, vitaminC, calcium, iron from items where type=\"breads\";" #ELE TRABALHA COM id =0, 1, O MEU EH O COMPLEXO
        #return str(restID)
        cursor.execute(qn)
        breads = cursor.fetchall()
        return breads

    def GET(self):
        ''' Return list of menus for restaurant restID'''

        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        try:
           breads = self.getDataFromDB()
        except mysql.connector.Error as e:
            logging.error(e)
            raise

        if output_format == 'text/html':
            a=1
        else:
            data = {}
            temp = []
            for bread in breads:
                tempBread = {"id":bread[0],
                                "name":bread[1],
                                "weight":bread[2],
                                "calories":bread[3],
                                "fatcals":bread[4],
                                "fat":bread[5],
                                "saturatedFat":bread[6],
                                "transFat":bread[7],
                                "cholesterol":bread[8],
                                "sodium":bread[9],
                                "carbos":bread[10],
                                "fibers":bread[11],
                                "sugars":bread[12],
                                "protein":bread[13],
                                "vitaminA":bread[14],
                                "vitaminC":bread[15],
                                "calcium":bread[16],
                                "iron":bread[17]
                                }
                temp.append(tempBread)

            data["entries"] = temp
            data["output"] = "success"
            return json.dumps(data, encoding='utf-8')

    def OPTIONS(self):
        return "<p>/restaurants/{restID}/menus/ allows GET, POST, and OPTIONS</p>"
