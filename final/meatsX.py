''' Implements handler for /menus
Imported from handler for /restaurants/{id} '''

import os, os.path, json, logging, mysql.connector
import apiutil
from apiutil import errorJSON
import cherrypy
from jinja2 import Environment, FileSystemLoader

#from categoryid import CategoryID
from meatid import MeatID

env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))

class Meats(object):
    ''' Handles resources /menus/{restID}
        Allowed methods: GET, POST, PUT, DELETE '''
    exposed = True

    def __init__(self):
        self.id = MeatID()
        self.db=dict()
        self.db['name']='DatSubway'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def getDataFromDB(self):
        cnx = mysql.connector.connect(
            user=self.db['user'],
            host=self.db['host'],
            database=self.db['name'],
        )
        cursor = cnx.cursor()
        qn="select id, name, weight, calories, fatcals, fat, saturatedFat, transFat, cholesterol, sodium, carbos, fibers, sugars, protein, vitaminA, vitaminC, calcium, iron from items where type=\"meats\";" #ELE TRABALHA COM id =0, 1, O MEU EH O COMPLEXO
        #return str(restID)
        cursor.execute(qn)
        meats = cursor.fetchall()
        return meats

    def GET(self):
        ''' Return list of menus for restaurant restID'''

        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        try:
           meats = self.getDataFromDB()
        except mysql.connector.Error as e:
            logging.error(e)
            raise

        if output_format == 'text/html':
            a=1
        else:
            data = {}
            temp = []
            for meat in meats:
                tempMeat = {"id":meat[0],
                                "name":meat[1],
                                "weight":meat[2],
                                "calories":meat[3],
                                "fatcals":meat[4],
                                "fat":meat[5],
                                "saturatedFat":meat[6],
                                "transFat":meat[7],
                                "cholesterol":meat[8],
                                "sodium":meat[9],
                                "carbos":meat[10],
                                "fibers":meat[11],
                                "sugars":meat[12],
                                "protein":meat[13],
                                "vitaminA":meat[14],
                                "vitaminC":meat[15],
                                "calcium":meat[16],
                                "iron":meat[17]
                                }
                temp.append(tempMeat)

            data["entries"] = temp
            data["output"] = "success"
            return json.dumps(data, encoding='utf-8') #MODIFICADO

    def OPTIONS(self):
        return "<p>/restaurants/{restID}/menus/ allows GET, POST, and OPTIONS</p>"
