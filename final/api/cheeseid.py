''' Implements handler for /menus
Imported from handler for /restaurants/{id} '''

import os, os.path, json, logging, mysql.connector
import apiutil
from apiutil import errorJSON
import cherrypy
from jinja2 import Environment, FileSystemLoader


env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))

class CheeseID(object):
    ''' Handles resources /menus/{restID}
        Allowed methods: GET, POST, PUT, DELETE '''
    exposed = True

    def __init__(self):
        self.db=dict()
        self.db['name']='DatSubway'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def getDataFromDB(self, cheeseid):
        cnx = mysql.connector.connect(
            user=self.db['user'],
            host=self.db['host'],
            database=self.db['name'],
        )

        cursor = cnx.cursor()
        qn="select name, weight, calories, fatcals, fat, saturatedFat, transFat, cholesterol, sodium, carbos, fibers, sugars, protein, vitaminA, vitaminC, calcium, iron from items where type=\"cheese\" and id=%s;" % cheeseid
        cursor.execute(qn)
        cheese = cursor.fetchone()
        return cheese

    def GET(self, cheeseid):
        ''' Return list of menus for restaurant restID'''

        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        cnx = mysql.connector.connect(
            user=self.db['user'],
            host=self.db['host'],
            database=self.db['name'],
        )
        cursor = cnx.cursor()
        q="select exists(select 1 from items where id=%s and type=\"cheese\");" % (cheeseid)
        cursor.execute(q)
        if not cursor.fetchall()[0][0]:
            return errorJSON(code=1001, message="Cheese with CheeseID=%s does not exist" % (cheeseid))

        try:
           cheese = self.getDataFromDB(cheeseid)
        except mysql.connector.Error as e:
            logging.error(e)
            raise

        if output_format == 'text/html':
            a=1
        else:
            data = {"name":cheese[0],
                    "weight":cheese[1],
                    "calories":cheese[2],
                    "fatcals":cheese[3],
                    "fat":cheese[4],
                    "saturatedFat":cheese[5],
                    "transFat":cheese[6],
                    "cholesterol":cheese[7],
                    "sodium":cheese[8],
                    "carbos":cheese[9],
                    "fibers":cheese[10],
                    "sugars":cheese[11],
                    "protein":cheese[12],
                    "vitaminA":cheese[13],
                    "vitaminC":cheese[14],
                    "calcium":cheese[15],
                    "iron":cheese[16]
                    }
            data["output"] = "success"
            return json.dumps(data, encoding='utf-8') #MODIFICADO

    def OPTIONS(self, cheeseid):
        return "<p>/restaurants/{restID}/menus/ allows GET, POST, and OPTIONS</p>"
