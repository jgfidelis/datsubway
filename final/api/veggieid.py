''' Implements handler for /menus
Imported from handler for /restaurants/{id} '''

import os, os.path, json, logging, mysql.connector
import apiutil
from apiutil import errorJSON
import cherrypy
from jinja2 import Environment, FileSystemLoader


env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))

class VeggieID(object):
    ''' Handles resources /menus/{restID}
        Allowed methods: GET, POST, PUT, DELETE '''
    exposed = True

    def __init__(self):
        self.db=dict()
        self.db['name']='DatSubway'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def getDataFromDB(self, veggieid):
        cnx = mysql.connector.connect(
            user=self.db['user'],
            host=self.db['host'],
            database=self.db['name'],
        )

        cursor = cnx.cursor()
        qn="select name, weight, calories, fatcals, fat, saturatedFat, transFat, cholesterol, sodium, carbos, fibers, sugars, protein, vitaminA, vitaminC, calcium, iron from items where type=\"vegetables\" and id=%s;" % veggieid
        cursor.execute(qn)
        veggie = cursor.fetchone()
        return veggie

    def GET(self, veggieid):
        ''' Return list of menus for restaurant restID'''

        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        cnx = mysql.connector.connect(
            user=self.db['user'],
            host=self.db['host'],
            database=self.db['name'],
        )
        cursor = cnx.cursor()
        q="select exists(select 1 from items where id=%s and type=\"vegetables\");" % (veggieid)
        cursor.execute(q)
        if not cursor.fetchall()[0][0]:
            return errorJSON(code=1004, message="Veggie with VeggieID=%s does not exist" % (veggieid))

        try:
           veggie = self.getDataFromDB(veggieid)
        except mysql.connector.Error as e:
            logging.error(e)
            raise

        if output_format == 'text/html':
            a=1
        else:
            data = {"name":veggie[0],
                    "weight":veggie[1],
                    "calories":veggie[2],
                    "fatcals":veggie[3],
                    "fat":veggie[4],
                    "saturatedFat":veggie[5],
                    "transFat":veggie[6],
                    "cholesterol":veggie[7],
                    "sodium":veggie[8],
                    "carbos":veggie[9],
                    "fibers":veggie[10],
                    "sugars":veggie[11],
                    "protein":veggie[12],
                    "vitaminA":veggie[13],
                    "vitaminC":veggie[14],
                    "calcium":veggie[15],
                    "iron":veggie[16]
                    }
            data["output"] = "success"
            return json.dumps(data, encoding='utf-8') #MODIFICADO

    def OPTIONS(self, veggieid):
        return "<p>/restaurants/{restID}/menus/ allows GET, POST, and OPTIONS</p>"
