''' /restaurants resource for feednd.com
This is run as a WSGI application through CherryPy and Apache with mod_wsgi
Author: Jesus A. Izaguirre, Ph.D.
Date: Feb. 17, 2015
Web Applications
Modified by Joel PC Filho
Date: March 26 of the same year.
'''
import apiutil
import sys
import os.path
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import threading
import cherrypy 
import os
import os.path
import math
import json
import mysql.connector 
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
import logging
from config import conf
from breads import Breads
from cheese import Cheese
from condiments import Condiments
from meats import Meats
from veggies import Veggies
from sub import Sub

class DatSubway(object):
	''' Handles resource /restaurants
		Allowed methods: GET, POST, OPTIONS '''
	exposed = True 

	def __init__(self):
		self.xtra = None
		self.breads = Breads()
		self.meats = Meats()
		self.cheese = Cheese()
		self.condiments = Condiments()
		self.veggies = Veggies()
		self.sub = Sub()

	def _cp_dispatch(self,vpath):
		print "Home._cp_dispatch with vpath: %s \n" % vpath
		#return vpath
		if len(vpath) == 1: # /{type}
			foodtype = str(vpath.pop(0))
			if foodtype == "breads":
				return self.breads
			if foodtype == "meats":
				return self.meats
			if foodtype == "cheese":
				return self.cheese
			if foodtype == "condiments":
				return self.condiments
			if foodtype == "veggies":
				return self.veggies
			if foodtype == "sub":
				return self.sub
		return vpath

	def GET(self):
		''' Get list of restaurants '''
		
		# Return data in the format requested in the Accept header
		# Fail with a status of 406 Not Acceptable if not HTML or JSON
		output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])
		self.data = dict()
		
		if output_format == 'text/html':
			return env.get_template('restaurants-tmpl.html').render(
				restaurants=dict(),
				info=self.xtra,
				base=cherrypy.request.base.rstrip('/') + '/'
			)
		else:
			return json.dumps(self.data, encoding='utf-8')

	def OPTIONS(self):
		''' Allows GET, POST, OPTIONS '''
		#Prepare response
		return "<p>/ allows GET</p>"

class StaticAssets(object):
	pass

print "NAME:", __name__
#if __name__ == '__main__' or __name__ == "_mod_wsgi_9dc6c4a71fc24457ccfa09b48efacf5f":
if __name__ == '__main__':
	print "Entrou no IF"
	conf = {
		'global': {
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
		},
		'/css': {
			'tools.staticdir.on': True,
			'tools.staticdir.dir': 'css'
		},
		'/js': {
			'tools.staticdir.on': True,
			'tools.staticdir.dir': 'js'
		}
	}
	cherrypy.tree.mount(DatSubway(), '/home', {
		'/': {
			'request.dispatch': cherrypy.dispatch.MethodDispatcher()
		}
	})
	cherrypy.tree.mount(StaticAssets(), '/', {
		'/': {
			'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
		},
		'/css': {
			'tools.staticdir.on': True,
			'tools.staticdir.dir': 'css'
		},
		'/js': {
			'tools.staticdir.on': True,
			'tools.staticdir.dir': 'js'
		}
	})
	cherrypy.engine.start()
	cherrypy.engine.block()
else:
	print "Entrou no Else"
	conf = {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
        }
    }
	application = cherrypy.Application(DatSubway(), None, conf)

