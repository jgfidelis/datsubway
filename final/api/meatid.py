''' Implements handler for /menus
Imported from handler for /restaurants/{id} '''

import os, os.path, json, logging, mysql.connector
import apiutil
from apiutil import errorJSON
import cherrypy
from jinja2 import Environment, FileSystemLoader


env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))

class MeatID(object):
    ''' Handles resources /menus/{restID}
        Allowed methods: GET, POST, PUT, DELETE '''
    exposed = True

    def __init__(self):
        self.db=dict()
        self.db['name']='DatSubway'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def getDataFromDB(self, meatid):
        cnx = mysql.connector.connect(
            user=self.db['user'],
            host=self.db['host'],
            database=self.db['name'],
        )

        cursor = cnx.cursor()
        qn="select name, weight, calories, fatcals, fat, saturatedFat, transFat, cholesterol, sodium, carbos, fibers, sugars, protein, vitaminA, vitaminC, calcium, iron from items where type=\"meats\" and id=%s;" % meatid
        cursor.execute(qn)
        meat = cursor.fetchone()
        return meat

    def GET(self, meatid):
        ''' Return list of menus for restaurant restID'''

        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        cnx = mysql.connector.connect(
            user=self.db['user'],
            host=self.db['host'],
            database=self.db['name'],
        )
        cursor = cnx.cursor()
        q="select exists(select 1 from items where id=%s and type=\"meats\");" % (meatid)
        cursor.execute(q)
        if not cursor.fetchall()[0][0]:
            return errorJSON(code=1003, message="Meat with MeatID=%s does not exist" % (meatid))

        try:
           meat = self.getDataFromDB(meatid)
        except mysql.connector.Error as e:
            logging.error(e)
            raise

        if output_format == 'text/html':
            data = {"name":meat[0],
                    "weight":meat[1],
                    "calories":meat[2],
                    "fatcals":meat[3],
                    "fat":meat[4],
                    "saturatedFat":meat[5],
                    "transFat":meat[6],
                    "cholesterol":meat[7],
                    "sodium":meat[8],
                    "carbos":meat[9],
                    "fibers":meat[10],
                    "sugars":meat[11],
                    "protein":meat[12],
                    "vitaminA":meat[13],
                    "vitaminC":meat[14],
                    "calcium":meat[15],
                    "iron":meat[16]
                    }
            data["output"] = "success"
            return json.dumps(data, encoding='utf-8')
        else:
            data = {"name":meat[0],
                    "weight":meat[1],
                    "calories":meat[2],
                    "fatcals":meat[3],
                    "fat":meat[4],
                    "saturatedFat":meat[5],
                    "transFat":meat[6],
                    "cholesterol":meat[7],
                    "sodium":meat[8],
                    "carbos":meat[9],
                    "fibers":meat[10],
                    "sugars":meat[11],
                    "protein":meat[12],
                    "vitaminA":meat[13],
                    "vitaminC":meat[14],
                    "calcium":meat[15],
                    "iron":meat[16]
                    }
            data["output"] = "success"
            return json.dumps(data, encoding='utf-8') #MODIFICADO

    def OPTIONS(self, meatid):
        return "<p>/restaurants/{restID}/menus/ allows GET, POST, and OPTIONS</p>"
