''' Implements handler for /menus
Imported from handler for /restaurants/{id} '''

import os, os.path, json, logging, mysql.connector
import apiutil
from apiutil import errorJSON
import cherrypy
from jinja2 import Environment, FileSystemLoader


env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))

class CondimentID(object):
    ''' Handles resources /menus/{restID}
        Allowed methods: GET, POST, PUT, DELETE '''
    exposed = True

    def __init__(self):
        self.db=dict()
        self.db['name']='DatSubway'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def getDataFromDB(self, condimentid):
        cnx = mysql.connector.connect(
            user=self.db['user'],
            host=self.db['host'],
            database=self.db['name'],
        )
        
        cursor = cnx.cursor()
        qn="select name, weight, calories, fatcals, fat, saturatedFat, transFat, cholesterol, sodium, carbos, fibers, sugars, protein, vitaminA, vitaminC, calcium, iron from items where type=\"condiments\" and id=%s;" % condimentid
        cursor.execute(qn)
        condiment = cursor.fetchone()
        return condiment

    def GET(self, condimentid):
        ''' Return list of menus for restaurant restID'''

        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        cnx = mysql.connector.connect(
            user=self.db['user'],
            host=self.db['host'],
            database=self.db['name'],
        )
        cursor = cnx.cursor()
        q="select exists(select 1 from items where id=%s and type=\"condiments\");" % (condimentid)
        cursor.execute(q)
        if not cursor.fetchall()[0][0]:
            return errorJSON(code=1002, message="Condiment with CondimentID=%s does not exist" % (condimentid))

        try:
           condiment = self.getDataFromDB(condimentid)
        except mysql.connector.Error as e:
            logging.error(e)
            raise

        if output_format == 'text/html':
            a=1
        else:
            data = {"name":condiment[0],
                    "weight":condiment[1],
                    "calories":condiment[2],
                    "fatcals":condiment[3],
                    "fat":condiment[4],
                    "saturatedFat":condiment[5],
                    "transFat":condiment[6],
                    "cholesterol":condiment[7],
                    "sodium":condiment[8],
                    "carbos":condiment[9],
                    "fibers":condiment[10],
                    "sugars":condiment[11],
                    "protein":condiment[12],
                    "vitaminA":condiment[13],
                    "vitaminC":condiment[14],
                    "calcium":condiment[15],
                    "iron":condiment[16]
                    }
            data["output"] = "success"
            return json.dumps(data, encoding='utf-8') #MODIFICADO

    def OPTIONS(self, condimentid):
        return "<p>/restaurants/{restID}/menus/ allows GET, POST, and OPTIONS</p>"
