#Use this script to populate the DatSubway database

import mysql.connector
import json
from decimal import *
import random

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'DatSubway'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
cursor = cnx.cursor()

#Load json files
inputFile = open('lang.json','r')
langDict = json.load(inputFile)
langDict = langDict['enUS']
inputFile.close()

inputFile = open('items.json','r')
itemsDict = json.load(inputFile)
inputFile.close()

for type, data in langDict.iteritems():
	names = data['names']
	nutritionData = itemsDict[type]
	for i in range(0,len(names)):
		nutrition = nutritionData[i]
		inputDict = {
			'type' 			: type,
			'name' 			: names[i],
			'weight'		: nutrition['servingSize'],
			'calories'		: nutrition['calories'],
			'fatcals'		: nutrition['calFromFat'],
			'fat'			: nutrition['totalFat'],
			'saturatedFat'	: nutrition['satFat'],
			'transFat'		: nutrition['transFat'],
			'cholesterol'	: nutrition['cholesterol'],
			'sodium'		: nutrition['sodium'],
			'carbos'		: nutrition['carbohydrate'],
			'fibers'		: nutrition['dietaryFiber'],
			'sugars'		: nutrition['sugars'],
			'protein'		: nutrition['protein'],
			'vitaminA'		: nutrition['vitaminA_DV'],
			'vitaminC'		: nutrition['vitaminC_DV'],
			'calcium'		: nutrition['calcium_DV'],
			'iron'			: nutrition['iron_DV']
		}
		addItem = ("""	INSERT INTO items (name,type, weight, calories, fatcals, fat, saturatedFat, transFat, cholesterol, sodium, carbos, fibers, sugars, protein, vitaminA, vitaminC, calcium, iron) 
						VALUES (%(name)s, %(type)s, %(weight)s, %(calories)s, %(fatcals)s, %(fat)s, %(saturatedFat)s, %(transFat)s, %(cholesterol)s, %(sodium)s, %(carbos)s, %(fibers)s, %(sugars)s, %(protein)s, %(vitaminA)s, %(vitaminC)s, %(calcium)s, %(iron)s)""")
		cursor.execute(addItem,inputDict)

cnx.commit()
cnx.close()

